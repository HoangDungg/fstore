﻿using FStore.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FStore.Controllers
{
    public class HomeController : Controller
    {
        private FStoreContext db;
        public HomeController()
        {
            db = new FStoreContext();
        }
        public ActionResult Index()
        {
            var product = db.Products.ToList();
            var viewModel = new HomeViewModel
            {
                Products = product,

            };
            return View(viewModel);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}