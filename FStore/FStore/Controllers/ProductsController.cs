﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FStore;
using FStore.Models;
using FStore.ViewModel;

namespace FStore.Controllers
{
    public class ProductsController : Controller
    {
        private FStoreContext db = new FStoreContext();

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        [Authorize]
        public ActionResult Create()
        {
            var result = new ProductViewModel
            {
                Categories = db.Categories.ToList(),
                Suppliers = db.Suppliers.ToList(),
            };
            return View(result);
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult Create(ProductViewModel product)
        {
            if (!ModelState.IsValid)
            {
                product.Categories = db.Categories.ToList();
                product.Suppliers = db.Suppliers.ToList();
                return View("Create", product);
            }
            var result = new Product
            {
                Name = product.Name,
                Desc = product.Desc,
                Price = product.Price,
                Image = product.Image,
                CategoryId = product.Category,
                SupplierId = product.Supplier
            };

            db.Products.Add(result);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Single(a => a.Id == id);
            var result = new ProductViewModel
            {
                Name = product.Name,
                Desc = product.Desc,
                Price = product.Price,
                Image = product.Image,
                Categories = db.Categories.ToList(),
                Suppliers = db.Suppliers.ToList(),
            };
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(result);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(ProductViewModel product)
        {
            //if (!ModelState.IsValid)
            //{
            //    product.Categories = db.Categories.ToList();
            //    product.Suppliers = db.Suppliers.ToList();
            //    return View("Edit", product);
            //}
            var productResult = db.Products.Single(a => a.Id == product.Id);

            productResult.Name = product.Name;
            productResult.Desc = product.Desc;
            productResult.Price = product.Price;
            productResult.Image = product.Image;
            productResult.SupplierId = product.Supplier;
            productResult.CategoryId = product.Category;

            //db.Entry(productResult).Property(a => a.Name).CurrentValue = product.Name;
            //db.Entry(productResult).Property(a => a.Desc).CurrentValue = product.Desc;
            //db.Entry(productResult).Property(a => a.Price).CurrentValue = product.Price;
            //db.Entry(productResult).Property(a => a.CategoryId).CurrentValue = product.CategoryId;
            //db.Entry(productResult).Property(a => a.SupplierId).CurrentValue = product.SupplierId;
            db.Entry(productResult).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index","Products");
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = db.Products.Find(id);
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
