﻿namespace FStore.Migrations
{
    using FStore.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FStore.FStoreContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FStore.FStoreContext context)
        {
            context.Categories.AddOrUpdate(
                new Category() { Id = 1, Name = "Tạ đơn" });
            context.Suppliers.AddOrUpdate(new Supplier() { Id = 1, Name = "asdasdasdasd" });
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
