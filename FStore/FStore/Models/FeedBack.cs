﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class FeedBack
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public int uId { get; set; }
        public ApplicationUser User { get; set; }
    }
}