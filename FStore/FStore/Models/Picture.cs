﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class Picture
    {
        public int Id { get; set; }
        public string Link { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
    }
}