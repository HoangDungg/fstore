﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class Supplier
    {
        public Supplier()
        {
            Product = new HashSet<Product>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public int Phone { get; set; }
        public string Email { get; set; }
        public DateTime? ContactDate { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}