﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class CartProduct
    {
        [Key]
        [Column(Order = 1)]
        public int ProductId { get; set; }
        public Product Product { get; set; }
        
        [Key]
        [Column(Order = 2)]
        public int CartId { get; set; }
        public Cart Cart { get; set; }
    }
}