﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class Order
    {
        public Order()
        {
            Products = new HashSet<Product>();
        }
        public int OrderId { get; set; }
        public ApplicationUser User { get; set; }
        public int Amount { get; set; }
        public DateTime? PaymentDate { get; set; }
        public float TotalPrice { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}