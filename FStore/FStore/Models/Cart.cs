﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.Models
{
    public class Cart
    {
        public Cart()
        {
            Product = new HashSet<Product>();
        }
        public int Id { get; set; }
        public int Amount { get; set; }
        public virtual ApplicationUser User { get; set; }
        public virtual ICollection<Product> Product { get; set; }
    }
}