﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.ViewModel
{
    public class CartViewModel
    {
        public int? Id { get; set; }
        public int Amount { get; set; }
        public int ProductId { get; set; }
    }
}