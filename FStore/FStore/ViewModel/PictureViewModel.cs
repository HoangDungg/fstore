﻿using FStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.ViewModel
{
    public class PictureViewModel
    {
        public string Link { get; set; }
        public int Product { get; set; }
        public IEnumerable<Product> Products { get; set; }
    }
}