﻿using FStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FStore.ViewModel
{
    public class HomeViewModel
    {
        public IEnumerable<Product> Products { get; set; }
        public string PictureLink { get; set; }
    }
}