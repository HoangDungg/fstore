﻿using FStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FStore.ViewModel
{
    public class ProductViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Tên Sản Phẩm")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Thông Tin Sản Phẩm")]
        public string Desc { get; set; }

        [Required]
        [Display(Name = "Giá Của Sản Phẩm")]
        public double Price { get; set; }

        [Required]
        [Display(Name = "Hình ảnh sản phẩm")]
        public string Image { get; set; }

        [Required]
        [Display(Name = "Nhà Cung Cấp Sản Phẩm")]
        public int Supplier { get; set; }
        public IEnumerable<Supplier> Suppliers { get; set; }

        [Required]
        [Display(Name = "Thể Loại Sản Phẩm")]

        public int Category { get; set; }
        public IEnumerable<Category> Categories { get; set; }
    }
}