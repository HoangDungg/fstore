﻿using FStore.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace FStore
{
    public class FStoreContext : IdentityDbContext<ApplicationUser>
    {
        public FStoreContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static FStoreContext Create()
        {
            return new FStoreContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().HasOptional(a => a.Cart).WithRequired(s => s.User);
            modelBuilder.Entity<ApplicationUser>().HasOptional(a => a.Order).WithRequired(s => s.User);
            modelBuilder.Entity<Product>()
                .HasMany<Cart>(a => a.Carts)
                .WithMany(a => a.Product)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProductId");
                    cs.MapRightKey("CartId");
                });
            modelBuilder.Entity<Product>()
                .HasMany<Order>(a => a.Orders)
                .WithMany(a => a.Products)
                .Map(cs =>
                {
                    cs.MapLeftKey("ProductId");
                    cs.MapRightKey("OrderId");
                });
            modelBuilder.Entity<Supplier>().HasMany<Product>(a => a.Product)
                .WithRequired(a => a.Supplier)
                .WillCascadeOnDelete();
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Picture> Picture { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<FeedBack> FeedBacks { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Tags> Tags { get; set; }
    }
}