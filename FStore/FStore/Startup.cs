﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FStore.Startup))]
namespace FStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
